# Angular 6 + Loopback 3
### ng-loopback-starter
Starting code base to build MEAN web applications using Angular 6 and Loopback 3

##### NOTE:
* By default, Loopback has route authorization enabled, which we may not want to handle when initially starting development. However, this should be re-enabled if your web application is going to be used in production!

    Within `server/boot/authentication.js` the following code is commented out:

      server.enableAuth();

* The client ng assets are served via `server/middleware.json`:

      "files": {
        "loopback#static": {
          "params": "$!../client"
        }
      },

* If you make changes to your Loopback models, update the sdk by running:

      go to the root directory of the project. If this was on f:\CareLabsGit\care-device-web, you'd do this:
      run ./node f:\CareLabsGit\care-device-web\bin\MyModelGen\bin\lb-cl-sdk server/server.js client/src/app/shared/sdk/ -w "enabled"

      If the project as running, as Kari said, "weird things are happening!"

      Documentation is here:
      https://github.com/mean-expert-official/loopback-sdk-builder/wiki

      (the only thing my version does is build the HTML and component declarations for us automatically. IF this is already done and checked in, yo uwould not need to run this.)

* Styling is provided by the Angular Material, for more information please read their documentation via<a href="https://material.angular.io/">https://material.angular.io/</a>

* Run npm install in root of the project as well as in the client directory

### npm run scripts: 

* **start:app** | uses concurrently to start client and server simultaneously
* **start:server** | starts server
* **start:client** | servers ng client
* **build:app** | builds ng client
* **build:sdk** | builds auto-generating sdk for LoopBack

JDG: I had better luck with opening 2 terminal sessions, and running:
```
cd ./client
ng serve
```

and on the root directory for the server, run:

```
node .
```
