-- --------------------------------------------------------
-- Host:                         192.168.0.201
-- Server version:               10.1.23-MariaDB-9+deb9u1 - Raspbian 9.0
-- Server OS:                    debian-linux-gnueabihf
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for users_db
CREATE DATABASE IF NOT EXISTS `users_db` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `users_db`;

-- Dumping structure for table users_db.people
CREATE TABLE IF NOT EXISTS `people` (
  `account_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `first_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `password` char(60) CHARACTER SET ascii DEFAULT NULL,
  `dob` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `clinical_trials` tinyint(1) DEFAULT NULL,
  `products_services` tinyint(1) DEFAULT NULL,
  `invocation_first` tinyint(1) NOT NULL DEFAULT '0',
  `timezone` char(5) CHARACTER SET utf8 NOT NULL DEFAULT 'PST',
  `alerts_on` bit(1) NOT NULL DEFAULT b'1',
  `smartphone` varchar(12) CHARACTER SET utf8 DEFAULT NULL,
  `voiceonlyphone` varchar(12) CHARACTER SET utf8 DEFAULT NULL,
  `last_edit` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_cloudsync` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `id_cloud` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1019 DEFAULT CHARSET=big5 ROW_FORMAT=COMPACT;

-- Dumping data for table users_db.people: ~11 rows (approximately)
/*!40000 ALTER TABLE `people` DISABLE KEYS */;
INSERT INTO `people` (`account_id`, `email`, `first_name`, `last_name`, `password`, `dob`, `clinical_trials`, `products_services`, `invocation_first`, `timezone`, `alerts_on`, `smartphone`, `voiceonlyphone`, `last_edit`, `last_cloudsync`, `id_cloud`) VALUES
	(1, 'j.gwinner@carelabs.healthcare', 'John', 'Gwinner', NULL, '4/21/1900', 1, 1, 1, 'PST', b'1', '310-227-9140', NULL, '2019-01-02 11:10:56', '1000-01-01 00:00:00', NULL),
	(2, 'john@gwinner.org', 'John', 'Gwinner, Sr', NULL, '4/21/1066', 0, 0, 0, 'PST', b'1', '310-227-9140', '310-891-3919', '2019-01-20 19:12:48', '1000-01-01 00:00:00', NULL),
	(3, 'john.gwinner@gmail.com', 'Doctor', 'Gwinner', NULL, '4/21/2000', 0, 0, 0, 'PST', b'1', '310-227-9140', '310-891-3919', '2019-01-20 19:14:42', '1000-01-01 00:00:00', NULL),
	(1004, 'anas_siddiqui@live.com', 'Muhammad', 'Anas', '$2b$10$RC555uaemTzyTR.vsj0eNO2dbTzLb6r1.aq7HhBinFyZEwabBUAqa', '1995-03-23', 0, 0, 0, '0', b'1', NULL, NULL, '2019-01-06 05:27:51', '1000-01-01 00:00:00', NULL),
	(1009, 'test@test.com', 'test', 'test#', '$2b$10$FbjSNx4VYwrXfwbHlw1.hOJL6hFqX4oZg18CcOtU2rrfz.dKVBxFe', '2018-06-04', NULL, NULL, 0, '0', b'1', NULL, NULL, '2019-01-06 05:27:51', '1000-01-01 00:00:00', NULL),
	(1013, 'jengle@usc.edu', 'Joshua', 'Engle', '$2b$10$TCzF4EnYwzelrQYr450r8.VqhubwwAur2lgl27fizjBZVCMi9bQU.', '1984-11-21', 1, 0, 0, '0', b'1', NULL, NULL, '2019-01-06 05:27:51', '1000-01-01 00:00:00', NULL),
	(1014, 'ot.ouyang@gmail.com', 'Yutong', 'Ouyang', '$2b$10$TtRvgCtx4CmChSY.KHPvzelar.5OnOfJVjEgk9DeSzSVZQ9kXDOo.', '1993-04-16', 1, 0, 0, '0', b'1', NULL, NULL, '2019-01-06 05:27:51', '1000-01-01 00:00:00', NULL),
	(1015, 'macengle7@gmail.com', 'Joshua', 'Engle', '$2b$10$Y0PUKuU6U1yS6uAEgPvNnO.ES7gHfE0HkKy/aifJeKPcmap.LVWAq', '1984-11-21', 1, 0, 1, '0', b'1', NULL, NULL, '2019-01-06 05:27:51', '1000-01-01 00:00:00', NULL),
	(1016, 'arnoldzhou96@gmail.com', 'Arnold', 'Zhou', '$2b$10$CLJiPE8PAX/MRQs/8N8ck.zAVyAXMzFeAoS3gOXjNngu61CK9pcn2', '1996-08-13', 0, 0, 0, '0', b'1', NULL, NULL, '2019-01-06 05:27:51', '1000-01-01 00:00:00', NULL),
	(1017, 'blackeyespanda@gmail.com', 'Clara', 'Wang', '$2b$10$xczMP3BFjflXHSMxtAQYOeD6TbCOHnn6zYJ0AlFYsXUpWf4C0Oqp.', '2018-05-26', 1, 0, 0, '0', b'1', NULL, NULL, '2019-01-06 05:27:51', '1000-01-01 00:00:00', NULL),
	(1018, 'anas_siddiqui@test.com', 'Muhammad', 'Anas', '$2b$10$/xUIZElZ.D8Egr/rIxJeYu62ZIGd6tNFq5W.tfrhSw7nIifUHoWGm', '2018-09-26', 1, 0, 1, '0', b'1', NULL, NULL, '2019-01-06 05:27:51', '1000-01-01 00:00:00', NULL);
/*!40000 ALTER TABLE `people` ENABLE KEYS */;

-- Dumping structure for table users_db.people_relations
CREATE TABLE IF NOT EXISTS `people_relations` (
  `patient_id` int(11) unsigned NOT NULL,
  `relation_id` int(11) unsigned NOT NULL,
  `role` enum('Unauthorized','Caregiver','Parent','Doctor','Administrator') NOT NULL DEFAULT 'Unauthorized',
  `authorize` enum('None','View','Edit','Delete') NOT NULL DEFAULT 'None',
  `lastmodified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_cloudsync` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `id_cloud` int(11) unsigned DEFAULT NULL,
  KEY `FK_user_relations_users` (`patient_id`),
  KEY `FK_user_relations_users_2` (`relation_id`),
  CONSTRAINT `people_relations_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `people` (`account_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `people_relations_ibfk_2` FOREIGN KEY (`relation_id`) REFERENCES `people` (`account_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='Relations from one user to another; for example, ID one may have a doctor of ID2 and a parent of ID3.';

-- Dumping data for table users_db.people_relations: ~2 rows (approximately)
/*!40000 ALTER TABLE `people_relations` DISABLE KEYS */;
INSERT INTO `people_relations` (`patient_id`, `relation_id`, `role`, `authorize`, `lastmodified`, `last_cloudsync`, `id_cloud`) VALUES
	(1, 2, 'Parent', 'Edit', '2019-01-20 19:17:02', '1000-01-01 00:00:00', NULL),
	(1, 3, 'Doctor', 'View', '2019-01-20 19:16:56', '1000-01-01 00:00:00', NULL);
/*!40000 ALTER TABLE `people_relations` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
