import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatTableModule,
  MatListModule,
  MatIconModule
} from "@angular/material";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//JDG: Note that many of the samples show HttpModule,
//BUT it's depreicated: See https://angular.io/api/http/HttpModule
//import { HttpModule }
//jdg: Use this instead
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from "./app.component";
//import { SDKBrowserModule } from "./shared/sdk/index";

import { AppRoutingModule } from './app-routing.module';

//our client/app objects (initially created via ng add component)
//basically, the pages in the app
import { AboutComponent } from './components/about/about.component';
import { SupportComponent } from './components/support/support.component';
import { NetsetupComponent } from './components/netsetup/netsetup.component';
import { DevicesetupComponent } from './components/devicesetup/devicesetup.component';
import { IncidentsComponent } from './components/incidents/incidents.component';
import { PersonsetupComponent } from './components/personsetup/personsetup.component';
import { PeoplelistComponent } from './components/peoplelist/peoplelist.component';
import { PatientsetupComponent } from './components/patientsetup/patientsetup.component';
import { HomeComponent } from './components/home/home.component';
import { FooterComponent } from './components/footer/footer.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { CamerainfoComponent } from './components/camerainfo/camerainfo.component';
import { RadarinfoComponent } from './components/radarinfo/radarinfo.component';
import { WearableinfoComponent } from './components/wearableinfo/wearableinfo.component';




@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    SupportComponent,
    NetsetupComponent,
    DevicesetupComponent,
    IncidentsComponent,
    PersonsetupComponent,
    PeoplelistComponent,
    PatientsetupComponent,
    HomeComponent,
    FooterComponent,
    NotFoundComponent,
    CamerainfoComponent,
    RadarinfoComponent,
    WearableinfoComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    CommonModule,
    FormsModule, //check if reactive or template or base
//    SDKBrowserModule.forRoot(),
    MatButtonModule,
    MatCheckboxModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatListModule,
    MatIconModule,
    ReactiveFormsModule,
    AppRoutingModule,
    // import HttpClientModule after BrowserModule.
    HttpClientModule    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
