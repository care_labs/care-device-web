import { Component } from '@angular/core';

// import { LoopBackConfig } from './shared/sdk/index';
import { FormGroup, FormBuilder } from '@angular/forms';

//jdg: Note from template; see why they custom imported Note instead of
//having Loopback handle that.
//import { NoteApi } from './shared/sdk/services/custom/Note';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public DeviceSetupForm: FormGroup;

  //TODO: Check why we have children in here ...
  // should stay at the top level.

  constructor(private fb: FormBuilder) {
    this.DeviceSetupForm = fb.group({
      devId: '',
      devName: '',
      devHostname:''
 
    })
  }


//  constructor(private na: NoteApi) {
//    LoopBackConfig.setBaseURL('http://127.0.0.1:3000');
//    LoopBackConfig.setApiVersion('api');
//  }
}
