import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevicesetupComponent } from './devicesetup.component';

describe('DevicesetupComponent', () => {
  let component: DevicesetupComponent;
  let fixture: ComponentFixture<DevicesetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevicesetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevicesetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
