import { Component, OnInit } from "@angular/core";
//jdg: wiring for Loopback, i.e. database access
//jdg: we already import these in app.component.ts
//import { LoopBackConfig } from '../../../../common/models'  ; // '../../../shared/sdk/index';
//import { FormBuilder, FormGroup } from "@angular/forms";
import { DeviceInfoInterface } from "../../models/DeviceInfo-interface"

//jdg: version in: "../../shared/sdk/models/DeviceInfo";
//is the "loopback client" way of doing things. Not needed currently.

import { DeviceInfoApiService } from "../../shared/sdk/services/custom/DeviceInfo";
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';

//import { LoopBackConfig } from "../../shared/sdk";
import { MatDatepickerModule } from '@angular/material/datepicker';

@Component({
  selector: "app-devicesetup",
  templateUrl: "./devicesetup.component.html",
  styleUrls: ["./devicesetup.component.css"]
})
export class DevicesetupComponent implements OnInit {


  //member variables
 
  userAlert = { msg: "" };


  constructor(
    public deviceApi: DeviceInfoApiService) { }
  //TTD: MUST CHANGE THIS! Make it a variable we setup if needed.
  //depends on configuration (dev vs. test)
  // Note that these are also in base.url.ts in the ./shared directory,
  // but placing thsi hear allows different API versions to be handled,
  // as wel as splitting upthe user database on a different physical server
  // for the cloud version. 
  //LoopBackConfig.setBaseURL("http://127.0.0.1:3000");
  //LoopBackConfig.setApiVersion("api");
  //this.createDeviceInfoForm();

  //jdg: this may be more OO
  //  private deviceInfo: DeviceInfo = new DeviceInfo();


  ngOnInit() {
    this.getDeviceInfo();
    console.log("DIAG: ngOnInit()", this.deviceApi.selectedDeviceInfo);
  }

  getDeviceInfo(): void {
    //  had a seperate copy of the current device;
    //  it's also at the API. Seems like the html.component doesn't have access to 'this' items? So we stuff it
    //  in a neutral location (seems to be best practice for a couple of examples)
    //  TTTD: double check this.
    this.deviceApi.getDeviceInfoById('1').subscribe((deviceInfo: DeviceInfoInterface) => (this.deviceApi.selectedDeviceInfo = deviceInfo));
    console.log("DIAG: getDeviceInfo()", this.deviceApi.selectedDeviceInfo);
  }

  onSaveDeviceInfo(deviceInfoForm: NgForm): void {
    console.log("DIAG: onSaveDeviceInfo()", this.deviceApi.selectedDeviceInfo);
    if (deviceInfoForm.value.id == null) {
      // NEW
      this.deviceApi.saveDeviceInfo(deviceInfoForm.value).subscribe(DeviceInfo => location.reload());
    } else {
      // update
      this.deviceApi.updateDeviceInfo(deviceInfoForm.value).subscribe(DeviceInfo => location.reload());
    }
  }
  doSaveDeviceInfo(deviceInfoForm: NgForm): void {

    if (deviceInfoForm == null) {
      console.log("DIAG: doSaveDeviceInfo() using base", this.deviceApi.selectedDeviceInfo, " in devicesetup.component.ts; null deviceInfoForm");
      this.deviceApi.updateDeviceInfo(this.deviceApi.selectedDeviceInfo).subscribe(DeviceInfo => location.reload());
    }
    else
    if (deviceInfoForm.value.id == null) {
      // NEW
      console.log("DIAG: doSaveDeviceInfo(), new", deviceInfoForm.value, "in devicesetup.component.ts; null device ID, have form");
      this.deviceApi.saveDeviceInfo(deviceInfoForm.value).subscribe(DeviceInfo => location.reload());
    } else {
      // update
      console.log("DIAG: doSaveDeviceInfo(), update", deviceInfoForm.value, "in devicesetup.component.ts; have form and id");
      this.deviceApi.updateDeviceInfo(deviceInfoForm.value).subscribe(DeviceInfo => location.reload());
    }
  }


}


