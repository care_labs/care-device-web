import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonsetupComponent } from './personsetup.component';

describe('PersonsetupComponent', () => {
  let component: PersonsetupComponent;
  let fixture: ComponentFixture<PersonsetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonsetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonsetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
