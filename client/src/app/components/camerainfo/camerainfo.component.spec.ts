import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CamerainfoComponent } from './camerainfo.component';

describe('CamerainfoComponent', () => {
  let component: CamerainfoComponent;
  let fixture: ComponentFixture<CamerainfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CamerainfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CamerainfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
