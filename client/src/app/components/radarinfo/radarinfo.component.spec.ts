import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RadarinfoComponent } from './radarinfo.component';

describe('RadarinfoComponent', () => {
  let component: RadarinfoComponent;
  let fixture: ComponentFixture<RadarinfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RadarinfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RadarinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
