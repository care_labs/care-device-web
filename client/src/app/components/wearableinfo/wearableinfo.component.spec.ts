import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WearableinfoComponent } from './wearableinfo.component';

describe('WearableinfoComponent', () => {
  let component: WearableinfoComponent;
  let fixture: ComponentFixture<WearableinfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WearableinfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WearableinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
