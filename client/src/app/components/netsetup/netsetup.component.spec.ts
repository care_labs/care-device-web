import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NetsetupComponent } from './netsetup.component';

describe('NetsetupComponent', () => {
  let component: NetsetupComponent;
  let fixture: ComponentFixture<NetsetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NetsetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NetsetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
