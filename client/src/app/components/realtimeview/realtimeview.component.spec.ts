import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RealtimeviewComponent } from './realtimeview.component';

describe('RealtimeviewComponent', () => {
  let component: RealtimeviewComponent;
  let fixture: ComponentFixture<RealtimeviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RealtimeviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RealtimeviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
