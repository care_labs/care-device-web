/* tslint:disable */

declare var Object: any;
export interface DeviceWifiInterface {
  "devId": number;
  "id": number;
  "passphrase": string;
  "ssid": string;
  "type": string;
  "dhcp": any;
  "staticip": number;
  "proxy": any;
  "proxyPort"?: number;
  "proxyHost"?: string;
  "lastcloudsync": Date;
  "idCloud"?: number;
}

export class DeviceWifi implements DeviceWifiInterface {
  "devId": number;
  "id": number;
  "passphrase": string;
  "ssid": string;
  "type": string;
  "dhcp": any;
  "staticip": number;
  "proxy": any;
  "proxyPort": number;
  "proxyHost": string;
  "lastcloudsync": Date;
  "idCloud": number;
  constructor(data?: DeviceWifiInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `DeviceWifi`.
   */
  public static getModelName() {
    return "DeviceWifi";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of DeviceWifi for dynamic purposes.
  **/
  public static factory(data: DeviceWifiInterface): DeviceWifi{
    return new DeviceWifi(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'DeviceWifi',
      plural: 'DeviceWifis',
      path: 'DeviceWifis',
      idName: 'id',
      properties: {
        "devId": {
          name: 'devId',
          type: 'number'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
        "passphrase": {
          name: 'passphrase',
          type: 'string'
        },
        "ssid": {
          name: 'ssid',
          type: 'string'
        },
        "type": {
          name: 'type',
          type: 'string'
        },
        "dhcp": {
          name: 'dhcp',
          type: 'any'
        },
        "staticip": {
          name: 'staticip',
          type: 'number'
        },
        "proxy": {
          name: 'proxy',
          type: 'any'
        },
        "proxyPort": {
          name: 'proxyPort',
          type: 'number'
        },
        "proxyHost": {
          name: 'proxyHost',
          type: 'string'
        },
        "lastcloudsync": {
          name: 'lastcloudsync',
          type: 'Date'
        },
        "idCloud": {
          name: 'idCloud',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
