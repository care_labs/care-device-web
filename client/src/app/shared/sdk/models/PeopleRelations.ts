/* tslint:disable */
import {
  People
} from './People';

declare var Object: any;
export interface PeopleRelationsInterface {
  "patientId": number;
  "relationId": number;
  "role": string;
  "authorize": string;
  "lastmodified": Date;
  "lastCloudsync": Date;
  "idCloud"?: number;
  "id"?: number;
  peopleRelationsIbfk1rel?: People;
  peopleRelationsIbfk2rel?: People;
}

export class PeopleRelations implements PeopleRelationsInterface {
  "patientId": number;
  "relationId": number;
  "role": string;
  "authorize": string;
  "lastmodified": Date;
  "lastCloudsync": Date;
  "idCloud": number;
  "id": number;
  peopleRelationsIbfk1rel: People;
  peopleRelationsIbfk2rel: People;
  constructor(data?: PeopleRelationsInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `PeopleRelations`.
   */
  public static getModelName() {
    return "PeopleRelations";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of PeopleRelations for dynamic purposes.
  **/
  public static factory(data: PeopleRelationsInterface): PeopleRelations{
    return new PeopleRelations(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'PeopleRelations',
      plural: 'PeopleRelations',
      path: 'PeopleRelations',
      idName: 'id',
      properties: {
        "patientId": {
          name: 'patientId',
          type: 'number'
        },
        "relationId": {
          name: 'relationId',
          type: 'number'
        },
        "role": {
          name: 'role',
          type: 'string'
        },
        "authorize": {
          name: 'authorize',
          type: 'string'
        },
        "lastmodified": {
          name: 'lastmodified',
          type: 'Date'
        },
        "lastCloudsync": {
          name: 'lastCloudsync',
          type: 'Date'
        },
        "idCloud": {
          name: 'idCloud',
          type: 'number'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
      },
      relations: {
        peopleRelationsIbfk1rel: {
          name: 'peopleRelationsIbfk1rel',
          type: 'People',
          model: 'People',
          relationType: 'belongsTo',
                  keyFrom: 'patientId',
          keyTo: 'accountId'
        },
        peopleRelationsIbfk2rel: {
          name: 'peopleRelationsIbfk2rel',
          type: 'People',
          model: 'People',
          relationType: 'belongsTo',
                  keyFrom: 'relationId',
          keyTo: 'accountId'
        },
      }
    }
  }
}
