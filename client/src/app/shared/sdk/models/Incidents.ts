/* tslint:disable */

declare var Object: any;
export interface IncidentsInterface {
  "id": number;
  "date"?: Date;
  "frequency"?: number;
  "caffeine"?: number;
  "medicationsSkipped"?: number;
  "abnormalLowSleep"?: number;
  "accountId"?: number;
  "createdOn"?: Date;
  "tonicsClonics"?: number;
  "tonics"?: number;
  "partials"?: number;
  "absence"?: number;
  "others"?: number;
  "seizuresFiveMinute"?: number;
  "notes"?: string;
  "duration"?: string;
  "accuracy"?: number;
  "averageHr"?: number;
  "averageRespiration"?: number;
  "gazeCaptured"?: any;
  "gazeAverage"?: number;
  "limbMotion"?: any;
  "lastCloudsync": Date;
  "idCloud"?: number;
}

export class Incidents implements IncidentsInterface {
  "id": number;
  "date": Date;
  "frequency": number;
  "caffeine": number;
  "medicationsSkipped": number;
  "abnormalLowSleep": number;
  "accountId": number;
  "createdOn": Date;
  "tonicsClonics": number;
  "tonics": number;
  "partials": number;
  "absence": number;
  "others": number;
  "seizuresFiveMinute": number;
  "notes": string;
  "duration": string;
  "accuracy": number;
  "averageHr": number;
  "averageRespiration": number;
  "gazeCaptured": any;
  "gazeAverage": number;
  "limbMotion": any;
  "lastCloudsync": Date;
  "idCloud": number;
  constructor(data?: IncidentsInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Incidents`.
   */
  public static getModelName() {
    return "Incidents";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Incidents for dynamic purposes.
  **/
  public static factory(data: IncidentsInterface): Incidents{
    return new Incidents(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Incidents',
      plural: 'Incidents',
      path: 'Incidents',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'number'
        },
        "date": {
          name: 'date',
          type: 'Date'
        },
        "frequency": {
          name: 'frequency',
          type: 'number'
        },
        "caffeine": {
          name: 'caffeine',
          type: 'number'
        },
        "medicationsSkipped": {
          name: 'medicationsSkipped',
          type: 'number'
        },
        "abnormalLowSleep": {
          name: 'abnormalLowSleep',
          type: 'number'
        },
        "accountId": {
          name: 'accountId',
          type: 'number'
        },
        "createdOn": {
          name: 'createdOn',
          type: 'Date'
        },
        "tonicsClonics": {
          name: 'tonicsClonics',
          type: 'number'
        },
        "tonics": {
          name: 'tonics',
          type: 'number'
        },
        "partials": {
          name: 'partials',
          type: 'number'
        },
        "absence": {
          name: 'absence',
          type: 'number'
        },
        "others": {
          name: 'others',
          type: 'number'
        },
        "seizuresFiveMinute": {
          name: 'seizuresFiveMinute',
          type: 'number'
        },
        "notes": {
          name: 'notes',
          type: 'string'
        },
        "duration": {
          name: 'duration',
          type: 'string'
        },
        "accuracy": {
          name: 'accuracy',
          type: 'number'
        },
        "averageHr": {
          name: 'averageHr',
          type: 'number'
        },
        "averageRespiration": {
          name: 'averageRespiration',
          type: 'number'
        },
        "gazeCaptured": {
          name: 'gazeCaptured',
          type: 'any'
        },
        "gazeAverage": {
          name: 'gazeAverage',
          type: 'number'
        },
        "limbMotion": {
          name: 'limbMotion',
          type: 'any'
        },
        "lastCloudsync": {
          name: 'lastCloudsync',
          type: 'Date'
        },
        "idCloud": {
          name: 'idCloud',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
