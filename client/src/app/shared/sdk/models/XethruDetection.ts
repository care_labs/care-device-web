/* tslint:disable */

declare var Object: any;
export interface XethruDetectionInterface {
  "id": number;
  "userId": number;
  "timeStamp": Date;
  "movementRepeating"?: number;
  "movementTime"?: number;
  "hasLegMoved"?: number;
  "lastCloudsync": Date;
  "idCloud"?: number;
}

export class XethruDetection implements XethruDetectionInterface {
  "id": number;
  "userId": number;
  "timeStamp": Date;
  "movementRepeating": number;
  "movementTime": number;
  "hasLegMoved": number;
  "lastCloudsync": Date;
  "idCloud": number;
  constructor(data?: XethruDetectionInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `XethruDetection`.
   */
  public static getModelName() {
    return "XethruDetection";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of XethruDetection for dynamic purposes.
  **/
  public static factory(data: XethruDetectionInterface): XethruDetection{
    return new XethruDetection(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'XethruDetection',
      plural: 'XethruDetections',
      path: 'XethruDetections',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'number'
        },
        "userId": {
          name: 'userId',
          type: 'number'
        },
        "timeStamp": {
          name: 'timeStamp',
          type: 'Date'
        },
        "movementRepeating": {
          name: 'movementRepeating',
          type: 'number'
        },
        "movementTime": {
          name: 'movementTime',
          type: 'number'
        },
        "hasLegMoved": {
          name: 'hasLegMoved',
          type: 'number'
        },
        "lastCloudsync": {
          name: 'lastCloudsync',
          type: 'Date'
        },
        "idCloud": {
          name: 'idCloud',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
