/* tslint:disable */

declare var Object: any;
export interface O2vibeConnectionInterface {
  "id": number;
  "userId": number;
  "timeStamp"?: Date;
  "heartRate"?: number;
  "spo2Rate"?: number;
  "lastCloudsync": Date;
  "idCloud"?: number;
}

export class O2vibeConnection implements O2vibeConnectionInterface {
  "id": number;
  "userId": number;
  "timeStamp": Date;
  "heartRate": number;
  "spo2Rate": number;
  "lastCloudsync": Date;
  "idCloud": number;
  constructor(data?: O2vibeConnectionInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `O2vibeConnection`.
   */
  public static getModelName() {
    return "O2vibeConnection";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of O2vibeConnection for dynamic purposes.
  **/
  public static factory(data: O2vibeConnectionInterface): O2vibeConnection{
    return new O2vibeConnection(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'O2vibeConnection',
      plural: 'O2vibeConnections',
      path: 'O2vibeConnections',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'number'
        },
        "userId": {
          name: 'userId',
          type: 'number'
        },
        "timeStamp": {
          name: 'timeStamp',
          type: 'Date'
        },
        "heartRate": {
          name: 'heartRate',
          type: 'number'
        },
        "spo2Rate": {
          name: 'spo2Rate',
          type: 'number'
        },
        "lastCloudsync": {
          name: 'lastCloudsync',
          type: 'Date'
        },
        "idCloud": {
          name: 'idCloud',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
