/* tslint:disable */

declare var Object: any;
export interface SeizuresInterface {
  "id": number;
  "date"?: string;
  "frequency"?: number;
  "caffeine"?: number;
  "medicationsSkipped"?: number;
  "abnormalLowSleep"?: number;
  "accountId"?: number;
  "createdOn"?: Date;
  "tonicsClonics"?: number;
  "tonics"?: number;
  "partials"?: number;
  "absence"?: number;
  "others"?: number;
  "seizuresFiveMinute"?: number;
  "lastCloudsync": Date;
  "idCloud"?: number;
}

export class Seizures implements SeizuresInterface {
  "id": number;
  "date": string;
  "frequency": number;
  "caffeine": number;
  "medicationsSkipped": number;
  "abnormalLowSleep": number;
  "accountId": number;
  "createdOn": Date;
  "tonicsClonics": number;
  "tonics": number;
  "partials": number;
  "absence": number;
  "others": number;
  "seizuresFiveMinute": number;
  "lastCloudsync": Date;
  "idCloud": number;
  constructor(data?: SeizuresInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Seizures`.
   */
  public static getModelName() {
    return "Seizures";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Seizures for dynamic purposes.
  **/
  public static factory(data: SeizuresInterface): Seizures{
    return new Seizures(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Seizures',
      plural: 'Seizures',
      path: 'Seizures',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'number'
        },
        "date": {
          name: 'date',
          type: 'string'
        },
        "frequency": {
          name: 'frequency',
          type: 'number'
        },
        "caffeine": {
          name: 'caffeine',
          type: 'number'
        },
        "medicationsSkipped": {
          name: 'medicationsSkipped',
          type: 'number'
        },
        "abnormalLowSleep": {
          name: 'abnormalLowSleep',
          type: 'number'
        },
        "accountId": {
          name: 'accountId',
          type: 'number'
        },
        "createdOn": {
          name: 'createdOn',
          type: 'Date'
        },
        "tonicsClonics": {
          name: 'tonicsClonics',
          type: 'number'
        },
        "tonics": {
          name: 'tonics',
          type: 'number'
        },
        "partials": {
          name: 'partials',
          type: 'number'
        },
        "absence": {
          name: 'absence',
          type: 'number'
        },
        "others": {
          name: 'others',
          type: 'number'
        },
        "seizuresFiveMinute": {
          name: 'seizuresFiveMinute',
          type: 'number'
        },
        "lastCloudsync": {
          name: 'lastCloudsync',
          type: 'Date'
        },
        "idCloud": {
          name: 'idCloud',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
