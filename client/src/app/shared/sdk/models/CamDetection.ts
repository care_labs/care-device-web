/* tslint:disable */

declare var Object: any;
export interface CamDetectionInterface {
  "id": number;
  "userId"?: number;
  "timeStamp": Date;
  "recDuration": Date;
  "recFilename"?: string;
  "lastCloudsync": Date;
  "idCloud"?: number;
}

export class CamDetection implements CamDetectionInterface {
  "id": number;
  "userId": number;
  "timeStamp": Date;
  "recDuration": Date;
  "recFilename": string;
  "lastCloudsync": Date;
  "idCloud": number;
  constructor(data?: CamDetectionInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `CamDetection`.
   */
  public static getModelName() {
    return "CamDetection";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of CamDetection for dynamic purposes.
  **/
  public static factory(data: CamDetectionInterface): CamDetection{
    return new CamDetection(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'CamDetection',
      plural: 'CamDetections',
      path: 'CamDetections',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'number'
        },
        "userId": {
          name: 'userId',
          type: 'number'
        },
        "timeStamp": {
          name: 'timeStamp',
          type: 'Date'
        },
        "recDuration": {
          name: 'recDuration',
          type: 'Date'
        },
        "recFilename": {
          name: 'recFilename',
          type: 'string'
        },
        "lastCloudsync": {
          name: 'lastCloudsync',
          type: 'Date'
        },
        "idCloud": {
          name: 'idCloud',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
