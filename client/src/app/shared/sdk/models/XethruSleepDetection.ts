/* tslint:disable */

declare var Object: any;
export interface XethruSleepDetectionInterface {
  "id": number;
  "userId": number;
  "timeStamp": Date;
  "breathingState"?: number;
  "state1Time"?: number;
  "breathingRate"?: number;
  "hasArmMoved"?: number;
  "movementTime"?: number;
  "movementRepeating"?: number;
  "lastCloudsync": Date;
  "idCloud"?: number;
}

export class XethruSleepDetection implements XethruSleepDetectionInterface {
  "id": number;
  "userId": number;
  "timeStamp": Date;
  "breathingState": number;
  "state1Time": number;
  "breathingRate": number;
  "hasArmMoved": number;
  "movementTime": number;
  "movementRepeating": number;
  "lastCloudsync": Date;
  "idCloud": number;
  constructor(data?: XethruSleepDetectionInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `XethruSleepDetection`.
   */
  public static getModelName() {
    return "XethruSleepDetection";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of XethruSleepDetection for dynamic purposes.
  **/
  public static factory(data: XethruSleepDetectionInterface): XethruSleepDetection{
    return new XethruSleepDetection(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'XethruSleepDetection',
      plural: 'XethruSleepDetections',
      path: 'XethruSleepDetections',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'number'
        },
        "userId": {
          name: 'userId',
          type: 'number'
        },
        "timeStamp": {
          name: 'timeStamp',
          type: 'Date'
        },
        "breathingState": {
          name: 'breathingState',
          type: 'number'
        },
        "state1Time": {
          name: 'state1Time',
          type: 'number'
        },
        "breathingRate": {
          name: 'breathingRate',
          type: 'number'
        },
        "hasArmMoved": {
          name: 'hasArmMoved',
          type: 'number'
        },
        "movementTime": {
          name: 'movementTime',
          type: 'number'
        },
        "movementRepeating": {
          name: 'movementRepeating',
          type: 'number'
        },
        "lastCloudsync": {
          name: 'lastCloudsync',
          type: 'Date'
        },
        "idCloud": {
          name: 'idCloud',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
