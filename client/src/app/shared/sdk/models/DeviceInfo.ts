/* tslint:disable */

declare var Object: any;
export interface DeviceInfoInterface {
  "id": string;
  "name": string;
  "hostname"?: string;
  "cpuid": string;
  "partno"?: string;
  "manufactured"?: Date;
  "lot"?: string;
  "udi"?: string;
  "addr1"?: string;
  "addr2"?: string;
  "city"?: string;
  "postalcode"?: string;
  "country"?: string;
  "timezone": string;
  "alertingenabled": string;
  "sensitivity": number;
  "reportTime": number;
  "rectimeMin": number;
  "reportAfter": number;
  "rectimePre": number;
  "debug": any;
  "localIpv4": number;
  "localIpv6": number;
  "lastUpdate": Date;
  "lastCloudsync": Date;
  "idCloud"?: number;
}

export class DeviceInfo implements DeviceInfoInterface {
  "id": string;
  "name": string;
  "hostname": string;
  "cpuid": string;
  "partno": string;
  "manufactured": Date;
  "lot": string;
  "udi": string;
  "addr1": string;
  "addr2": string;
  "city": string;
  "postalcode": string;
  "country": string;
  "timezone": string;
  "alertingenabled": string;
  "sensitivity": number;
  "reportTime": number;
  "rectimeMin": number;
  "reportAfter": number;
  "rectimePre": number;
  "debug": any;
  "localIpv4": number;
  "localIpv6": number;
  "lastUpdate": Date;
  "lastCloudsync": Date;
  "idCloud": number;
  constructor(data?: DeviceInfoInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `DeviceInfo`.
   */
  public static getModelName() {
    return "DeviceInfo";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of DeviceInfo for dynamic purposes.
  **/
  public static factory(data: DeviceInfoInterface): DeviceInfo{
    return new DeviceInfo(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'DeviceInfo',
      plural: 'DeviceInfos',
      path: 'DeviceInfos',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'string'
        },
        "name": {
          name: 'name',
          type: 'string'
        },
        "hostname": {
          name: 'hostname',
          type: 'string'
        },
        "cpuid": {
          name: 'cpuid',
          type: 'string'
        },
        "partno": {
          name: 'Partno',
          type: 'string'
        },
        "manufactured": {
          name: 'manufactured',
          type: 'Date'
        },
        "lot": {
          name: 'lot',
          type: 'string'
        },
        "udi": {
          name: 'udi',
          type: 'string'
        },
        "addr1": {
          name: 'addr1',
          type: 'string'
        },
        "addr2": {
          name: 'addr2',
          type: 'string'
        },
        "city": {
          name: 'city',
          type: 'string'
        },
        "postalcode": {
          name: 'postalcode',
          type: 'string'
        },
        "country": {
          name: 'country',
          type: 'string'
        },
        "timezone": {
          name: 'timezone',
          type: 'string'
        },
        "alertingenabled": {
          name: 'alertingenabled',
          type: 'string'
        },
        "sensitivity": {
          name: 'sensitivity',
          type: 'number'
        },
        "reportTime": {
          name: 'reportTime',
          type: 'number'
        },
        "rectimeMin": {
          name: 'rectimeMin',
          type: 'number'
        },
        "reportAfter": {
          name: 'reportAfter',
          type: 'number'
        },
        "rectimePre": {
          name: 'rectimePre',
          type: 'number'
        },
        "debug": {
          name: 'debug',
          type: 'any'
        },
        "localIpv4": {
          name: 'localIpv4',
          type: 'number'
        },
        "localIpv6": {
          name: 'localIpv6',
          type: 'number'
        },
        "lastUpdate": {
          name: 'lastUpdate',
          type: 'Date'
        },
        "lastCloudsync": {
          name: 'lastCloudsync',
          type: 'Date'
        },
        "idCloud": {
          name: 'idCloud',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
