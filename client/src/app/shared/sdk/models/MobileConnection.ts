/* tslint:disable */

declare var Object: any;
export interface MobileConnectionInterface {
  "id": number;
  "userId": number;
  "timeStamp": Date;
  "heartRate"?: number;
  "accelerationx"?: number;
  "accelerationy"?: number;
  "accelerationz"?: number;
  "seizureDetected": number;
  "lastCloudsync"?: Date;
  "idCloud"?: number;
}

export class MobileConnection implements MobileConnectionInterface {
  "id": number;
  "userId": number;
  "timeStamp": Date;
  "heartRate": number;
  "accelerationx": number;
  "accelerationy": number;
  "accelerationz": number;
  "seizureDetected": number;
  "lastCloudsync": Date;
  "idCloud": number;
  constructor(data?: MobileConnectionInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `MobileConnection`.
   */
  public static getModelName() {
    return "MobileConnection";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of MobileConnection for dynamic purposes.
  **/
  public static factory(data: MobileConnectionInterface): MobileConnection{
    return new MobileConnection(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'MobileConnection',
      plural: 'MobileConnections',
      path: 'MobileConnections',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'number'
        },
        "userId": {
          name: 'userId',
          type: 'number'
        },
        "timeStamp": {
          name: 'timeStamp',
          type: 'Date'
        },
        "heartRate": {
          name: 'heartRate',
          type: 'number'
        },
        "accelerationx": {
          name: 'accelerationx',
          type: 'number'
        },
        "accelerationy": {
          name: 'accelerationy',
          type: 'number'
        },
        "accelerationz": {
          name: 'accelerationz',
          type: 'number'
        },
        "seizureDetected": {
          name: 'seizureDetected',
          type: 'number'
        },
        "lastCloudsync": {
          name: 'lastCloudsync',
          type: 'Date'
        },
        "idCloud": {
          name: 'idCloud',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
