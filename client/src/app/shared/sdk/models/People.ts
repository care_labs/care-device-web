/* tslint:disable */

declare var Object: any;
export interface PeopleInterface {
  "accountId": number;
  "email": string;
  "firstName"?: string;
  "lastName"?: string;
  "password"?: string;
  "dob"?: string;
  "clinicalTrials"?: number;
  "productsServices"?: number;
  "invocationFirst": number;
  "timezone": string;
  "alertsOn": any;
  "smartphone"?: string;
  "voiceonlyphone"?: string;
  "lastEdit": Date;
  "lastCloudsync": Date;
  "idCloud"?: number;
}

export class People implements PeopleInterface {
  "accountId": number;
  "email": string;
  "firstName": string;
  "lastName": string;
  "password": string;
  "dob": string;
  "clinicalTrials": number;
  "productsServices": number;
  "invocationFirst": number;
  "timezone": string;
  "alertsOn": any;
  "smartphone": string;
  "voiceonlyphone": string;
  "lastEdit": Date;
  "lastCloudsync": Date;
  "idCloud": number;
  constructor(data?: PeopleInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `People`.
   */
  public static getModelName() {
    return "People";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of People for dynamic purposes.
  **/
  public static factory(data: PeopleInterface): People{
    return new People(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'People',
      plural: 'People',
      path: 'People',
      idName: 'accountId',
      properties: {
        "accountId": {
          name: 'accountId',
          type: 'number'
        },
        "email": {
          name: 'email',
          type: 'string'
        },
        "firstName": {
          name: 'firstName',
          type: 'string'
        },
        "lastName": {
          name: 'lastName',
          type: 'string'
        },
        "password": {
          name: 'password',
          type: 'string'
        },
        "dob": {
          name: 'dob',
          type: 'string'
        },
        "clinicalTrials": {
          name: 'clinicalTrials',
          type: 'number'
        },
        "productsServices": {
          name: 'productsServices',
          type: 'number'
        },
        "invocationFirst": {
          name: 'invocationFirst',
          type: 'number'
        },
        "timezone": {
          name: 'timezone',
          type: 'string'
        },
        "alertsOn": {
          name: 'alertsOn',
          type: 'any'
        },
        "smartphone": {
          name: 'smartphone',
          type: 'string'
        },
        "voiceonlyphone": {
          name: 'voiceonlyphone',
          type: 'string'
        },
        "lastEdit": {
          name: 'lastEdit',
          type: 'Date'
        },
        "lastCloudsync": {
          name: 'lastCloudsync',
          type: 'Date'
        },
        "idCloud": {
          name: 'idCloud',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
