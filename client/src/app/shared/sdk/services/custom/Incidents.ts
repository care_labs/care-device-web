/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';

// JDG: TTD: Need to build the below file automatically also; current one is in the
// wrong place, as it was hand edited. Should go in ../../models so it's in
// machine generated folder (SDK)
import { IncidentsInterface } from '../../../../models/Incidents-interface';

import { AuthService } from '../../../../services/auth.service';
/**
 * Api services for the `Incidents` model.

  JDG: Modified per the "Incidentss" sample; @Mean expert wasn't generating all HTTP verbs.
 */
@Injectable({
  providedIn: 'root'
})
export class IncidentsApiService {

  constructor( private http: HttpClient, private authService: AuthService) {
  }

  Incidents: Observable<any>;
  Incidentss: Observable<any>;
  
  public selectedIncidents: IncidentsInterface = {
    
    id: 0,
    date: new Date(0),
    frequency: 0,
    caffeine: 0,
    medicationsSkipped: 0,
    abnormalLowSleep: 0,
    accountId: 0,
    createdOn: new Date(0),
    tonicsClonics: 0,
    tonics: 0,
    partials: 0,
    absence: 0,
    others: 0,
    seizuresFiveMinute: 0,
    notes: '',
    duration: '',
    accuracy: 0,
    averageHr: 0,
    averageRespiration: 0,
    gazeCaptured: <any>null,
    gazeAverage: 0,
    limbMotion: <any>null,
    lastCloudsync: new Date(0),
    idCloud: 0,
      } ;

headers: HttpHeaders = new HttpHeaders({
  'Content-Type': 'application/json',
  Authorization: this.authService.getToken()
});

getAllIncidents () {
  const url_api = `http://localhost:3000/api/Incidents`;
  return this.http.get(url_api);
}
getIncidentsById (id: string) {
  const url_api = `http://localhost:3000/api/Incidentss/${id}`;
  return (this.Incidents = this.http.get(url_api));
}

saveIncidents(Incidents: IncidentsInterface) {
  // TODO: obtain token
  // TODO: not null
  const token = this.authService.getToken();
  const url_api = `http://localhost:3000/api/Incidentss?access_token=${token}`;
  return this.http
    .post<IncidentsInterface>(url_api, Incidents, { headers: this.headers })
    .pipe(map(data => data));
}

updateIncidents(Incidents) {
  // TODO: obtener token
  // TODO: not null
  const IncidentsId = Incidents.IncidentsId;
  const token = this.authService.getToken();
  const url_api = `http://localhost:3000/api/Incidentss/${IncidentsId}/?access_token=${token}`;
  return this.http
    .put<IncidentsInterface>(url_api, Incidents, { headers: this.headers })
    .pipe(map(data => data));
}

deleteIncidents(id: string) {
  // TODO: obtener token
  // TODO: not null
  const token = this.authService.getToken();
  console.log(token);
  const url_api = `http://localhost:3000/api/Incidentss/${id}/?access_token=${token}`;
  return this.http
    .delete<IncidentsInterface>(url_api, { headers: this.headers })
    .pipe(map(data => data));
}
}
