/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';

// JDG: TTD: Need to build the below file automatically also; current one is in the
// wrong place, as it was hand edited. Should go in ../../models so it's in
// machine generated folder (SDK)
import { MobileConnectionInterface } from '../../../../models/MobileConnection-interface';

import { AuthService } from '../../../../services/auth.service';
/**
 * Api services for the `MobileConnection` model.

  JDG: Modified per the "MobileConnections" sample; @Mean expert wasn't generating all HTTP verbs.
 */
@Injectable({
  providedIn: 'root'
})
export class MobileConnectionApiService {

  constructor( private http: HttpClient, private authService: AuthService) {
  }

  MobileConnection: Observable<any>;
  MobileConnections: Observable<any>;
  
  public selectedMobileConnection: MobileConnectionInterface = {
    
    id: 0,
    userId: 0,
    timeStamp: new Date(0),
    heartRate: 0,
    accelerationx: 0,
    accelerationy: 0,
    accelerationz: 0,
    seizureDetected: 0,
    lastCloudsync: new Date(0),
    idCloud: 0,
      } ;

headers: HttpHeaders = new HttpHeaders({
  'Content-Type': 'application/json',
  Authorization: this.authService.getToken()
});

getAllMobileConnection () {
  const url_api = `http://localhost:3000/api/MobileConnection`;
  return this.http.get(url_api);
}
getMobileConnectionById (id: string) {
  const url_api = `http://localhost:3000/api/MobileConnections/${id}`;
  return (this.MobileConnection = this.http.get(url_api));
}

saveMobileConnection(MobileConnection: MobileConnectionInterface) {
  // TODO: obtain token
  // TODO: not null
  const token = this.authService.getToken();
  const url_api = `http://localhost:3000/api/MobileConnections?access_token=${token}`;
  return this.http
    .post<MobileConnectionInterface>(url_api, MobileConnection, { headers: this.headers })
    .pipe(map(data => data));
}

updateMobileConnection(MobileConnection) {
  // TODO: obtener token
  // TODO: not null
  const MobileConnectionId = MobileConnection.MobileConnectionId;
  const token = this.authService.getToken();
  const url_api = `http://localhost:3000/api/MobileConnections/${MobileConnectionId}/?access_token=${token}`;
  return this.http
    .put<MobileConnectionInterface>(url_api, MobileConnection, { headers: this.headers })
    .pipe(map(data => data));
}

deleteMobileConnection(id: string) {
  // TODO: obtener token
  // TODO: not null
  const token = this.authService.getToken();
  console.log(token);
  const url_api = `http://localhost:3000/api/MobileConnections/${id}/?access_token=${token}`;
  return this.http
    .delete<MobileConnectionInterface>(url_api, { headers: this.headers })
    .pipe(map(data => data));
}
}
