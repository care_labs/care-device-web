/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';

// JDG: TTD: Need to build the below file automatically also; current one is in the
// wrong place, as it was hand edited. Should go in ../../models so it's in
// machine generated folder (SDK)
import { DeviceWifiInterface } from '../../../../models/DeviceWifi-interface';

import { AuthService } from '../../../../services/auth.service';
/**
 * Api services for the `DeviceWifi` model.

  JDG: Modified per the "DeviceWifis" sample; @Mean expert wasn't generating all HTTP verbs.
 */
@Injectable({
  providedIn: 'root'
})
export class DeviceWifiApiService {

  constructor( private http: HttpClient, private authService: AuthService) {
  }

  DeviceWifi: Observable<any>;
  DeviceWifis: Observable<any>;
  
  public selectedDeviceWifi: DeviceWifiInterface = {
    
    devId: 0,
    id: 0,
    passphrase: '',
    ssid: '',
    type: '',
    dhcp: <any>null,
    staticip: 0,
    proxy: <any>null,
    proxyPort: 0,
    proxyHost: '',
    lastcloudsync: new Date(0),
    idCloud: 0,
      } ;

headers: HttpHeaders = new HttpHeaders({
  'Content-Type': 'application/json',
  Authorization: this.authService.getToken()
});

getAllDeviceWifi () {
  const url_api = `http://localhost:3000/api/DeviceWifi`;
  return this.http.get(url_api);
}
getDeviceWifiById (id: string) {
  const url_api = `http://localhost:3000/api/DeviceWifis/${id}`;
  return (this.DeviceWifi = this.http.get(url_api));
}

saveDeviceWifi(DeviceWifi: DeviceWifiInterface) {
  // TODO: obtain token
  // TODO: not null
  const token = this.authService.getToken();
  const url_api = `http://localhost:3000/api/DeviceWifis?access_token=${token}`;
  return this.http
    .post<DeviceWifiInterface>(url_api, DeviceWifi, { headers: this.headers })
    .pipe(map(data => data));
}

updateDeviceWifi(DeviceWifi) {
  // TODO: obtener token
  // TODO: not null
  const DeviceWifiId = DeviceWifi.DeviceWifiId;
  const token = this.authService.getToken();
  const url_api = `http://localhost:3000/api/DeviceWifis/${DeviceWifiId}/?access_token=${token}`;
  return this.http
    .put<DeviceWifiInterface>(url_api, DeviceWifi, { headers: this.headers })
    .pipe(map(data => data));
}

deleteDeviceWifi(id: string) {
  // TODO: obtener token
  // TODO: not null
  const token = this.authService.getToken();
  console.log(token);
  const url_api = `http://localhost:3000/api/DeviceWifis/${id}/?access_token=${token}`;
  return this.http
    .delete<DeviceWifiInterface>(url_api, { headers: this.headers })
    .pipe(map(data => data));
}
}
