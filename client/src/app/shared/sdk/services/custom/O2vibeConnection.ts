/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';

// JDG: TTD: Need to build the below file automatically also; current one is in the
// wrong place, as it was hand edited. Should go in ../../models so it's in
// machine generated folder (SDK)
import { O2vibeConnectionInterface } from '../../../../models/O2vibeConnection-interface';

import { AuthService } from '../../../../services/auth.service';
/**
 * Api services for the `O2vibeConnection` model.

  JDG: Modified per the "O2vibeConnections" sample; @Mean expert wasn't generating all HTTP verbs.
 */
@Injectable({
  providedIn: 'root'
})
export class O2vibeConnectionApiService {

  constructor( private http: HttpClient, private authService: AuthService) {
  }

  O2vibeConnection: Observable<any>;
  O2vibeConnections: Observable<any>;
  
  public selectedO2vibeConnection: O2vibeConnectionInterface = {
    
    id: 0,
    userId: 0,
    timeStamp: new Date(0),
    heartRate: 0,
    spo2Rate: 0,
    lastCloudsync: new Date(0),
    idCloud: 0,
      } ;

headers: HttpHeaders = new HttpHeaders({
  'Content-Type': 'application/json',
  Authorization: this.authService.getToken()
});

getAllO2vibeConnection () {
  const url_api = `http://localhost:3000/api/O2vibeConnection`;
  return this.http.get(url_api);
}
getO2vibeConnectionById (id: string) {
  const url_api = `http://localhost:3000/api/O2vibeConnections/${id}`;
  return (this.O2vibeConnection = this.http.get(url_api));
}

saveO2vibeConnection(O2vibeConnection: O2vibeConnectionInterface) {
  // TODO: obtain token
  // TODO: not null
  const token = this.authService.getToken();
  const url_api = `http://localhost:3000/api/O2vibeConnections?access_token=${token}`;
  return this.http
    .post<O2vibeConnectionInterface>(url_api, O2vibeConnection, { headers: this.headers })
    .pipe(map(data => data));
}

updateO2vibeConnection(O2vibeConnection) {
  // TODO: obtener token
  // TODO: not null
  const O2vibeConnectionId = O2vibeConnection.O2vibeConnectionId;
  const token = this.authService.getToken();
  const url_api = `http://localhost:3000/api/O2vibeConnections/${O2vibeConnectionId}/?access_token=${token}`;
  return this.http
    .put<O2vibeConnectionInterface>(url_api, O2vibeConnection, { headers: this.headers })
    .pipe(map(data => data));
}

deleteO2vibeConnection(id: string) {
  // TODO: obtener token
  // TODO: not null
  const token = this.authService.getToken();
  console.log(token);
  const url_api = `http://localhost:3000/api/O2vibeConnections/${id}/?access_token=${token}`;
  return this.http
    .delete<O2vibeConnectionInterface>(url_api, { headers: this.headers })
    .pipe(map(data => data));
}
}
