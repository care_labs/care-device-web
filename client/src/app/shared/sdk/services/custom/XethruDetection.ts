/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';

// JDG: TTD: Need to build the below file automatically also; current one is in the
// wrong place, as it was hand edited. Should go in ../../models so it's in
// machine generated folder (SDK)
import { XethruDetectionInterface } from '../../../../models/XethruDetection-interface';

import { AuthService } from '../../../../services/auth.service';
/**
 * Api services for the `XethruDetection` model.

  JDG: Modified per the "XethruDetections" sample; @Mean expert wasn't generating all HTTP verbs.
 */
@Injectable({
  providedIn: 'root'
})
export class XethruDetectionApiService {

  constructor( private http: HttpClient, private authService: AuthService) {
  }

  XethruDetection: Observable<any>;
  XethruDetections: Observable<any>;
  
  public selectedXethruDetection: XethruDetectionInterface = {
    
    id: 0,
    userId: 0,
    timeStamp: new Date(0),
    movementRepeating: 0,
    movementTime: 0,
    hasLegMoved: 0,
    lastCloudsync: new Date(0),
    idCloud: 0,
      } ;

headers: HttpHeaders = new HttpHeaders({
  'Content-Type': 'application/json',
  Authorization: this.authService.getToken()
});

getAllXethruDetection () {
  const url_api = `http://localhost:3000/api/XethruDetection`;
  return this.http.get(url_api);
}
getXethruDetectionById (id: string) {
  const url_api = `http://localhost:3000/api/XethruDetections/${id}`;
  return (this.XethruDetection = this.http.get(url_api));
}

saveXethruDetection(XethruDetection: XethruDetectionInterface) {
  // TODO: obtain token
  // TODO: not null
  const token = this.authService.getToken();
  const url_api = `http://localhost:3000/api/XethruDetections?access_token=${token}`;
  return this.http
    .post<XethruDetectionInterface>(url_api, XethruDetection, { headers: this.headers })
    .pipe(map(data => data));
}

updateXethruDetection(XethruDetection) {
  // TODO: obtener token
  // TODO: not null
  const XethruDetectionId = XethruDetection.XethruDetectionId;
  const token = this.authService.getToken();
  const url_api = `http://localhost:3000/api/XethruDetections/${XethruDetectionId}/?access_token=${token}`;
  return this.http
    .put<XethruDetectionInterface>(url_api, XethruDetection, { headers: this.headers })
    .pipe(map(data => data));
}

deleteXethruDetection(id: string) {
  // TODO: obtener token
  // TODO: not null
  const token = this.authService.getToken();
  console.log(token);
  const url_api = `http://localhost:3000/api/XethruDetections/${id}/?access_token=${token}`;
  return this.http
    .delete<XethruDetectionInterface>(url_api, { headers: this.headers })
    .pipe(map(data => data));
}
}
