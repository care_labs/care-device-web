/* tslint:disable */
import { Injectable } from '@angular/core';
import { CamDetection } from '../../models/CamDetection';
import { DeviceInfo } from '../../models/DeviceInfo';
import { DeviceWifi } from '../../models/DeviceWifi';
import { Incidents } from '../../models/Incidents';
import { MobileConnection } from '../../models/MobileConnection';
import { O2vibeConnection } from '../../models/O2vibeConnection';
import { XethruDetection } from '../../models/XethruDetection';
import { XethruSleepDetection } from '../../models/XethruSleepDetection';
import { People } from '../../models/People';
import { PeopleRelations } from '../../models/PeopleRelations';
import { Seizures } from '../../models/Seizures';

export interface Models { [name: string]: any }

@Injectable()
export class SDKModels {

  private models: Models = {
    CamDetection: CamDetection,
    DeviceInfo: DeviceInfo,
    DeviceWifi: DeviceWifi,
    Incidents: Incidents,
    MobileConnection: MobileConnection,
    O2vibeConnection: O2vibeConnection,
    XethruDetection: XethruDetection,
    XethruSleepDetection: XethruSleepDetection,
    People: People,
    PeopleRelations: PeopleRelations,
    Seizures: Seizures,
    
  };

  public get(modelName: string): any {
    return this.models[modelName];
  }

  public getAll(): Models {
    return this.models;
  }

  public getModelNames(): string[] {
    return Object.keys(this.models);
  }
}
