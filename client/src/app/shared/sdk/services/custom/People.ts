/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';

// JDG: TTD: Need to build the below file automatically also; current one is in the
// wrong place, as it was hand edited. Should go in ../../models so it's in
// machine generated folder (SDK)
import { PeopleInterface } from '../../../../models/People-interface';

import { AuthService } from '../../../../services/auth.service';
/**
 * Api services for the `People` model.

  JDG: Modified per the "Peoples" sample; @Mean expert wasn't generating all HTTP verbs.
 */
@Injectable({
  providedIn: 'root'
})
export class PeopleApiService {

  constructor( private http: HttpClient, private authService: AuthService) {
  }

  People: Observable<any>;
  Peoples: Observable<any>;
  
  public selectedPeople: PeopleInterface = {
    
    accountId: 0,
    email: '',
    firstName: '',
    lastName: '',
    password: '',
    dob: '',
    clinicalTrials: 0,
    productsServices: 0,
    invocationFirst: 0,
    timezone: '',
    alertsOn: <any>null,
    smartphone: '',
    voiceonlyphone: '',
    lastEdit: new Date(0),
    lastCloudsync: new Date(0),
    idCloud: 0,
      } ;

headers: HttpHeaders = new HttpHeaders({
  'Content-Type': 'application/json',
  Authorization: this.authService.getToken()
});

getAllPeople () {
  const url_api = `http://localhost:3000/api/People`;
  return this.http.get(url_api);
}
getPeopleById (id: string) {
  const url_api = `http://localhost:3000/api/Peoples/${id}`;
  return (this.People = this.http.get(url_api));
}

savePeople(People: PeopleInterface) {
  // TODO: obtain token
  // TODO: not null
  const token = this.authService.getToken();
  const url_api = `http://localhost:3000/api/Peoples?access_token=${token}`;
  return this.http
    .post<PeopleInterface>(url_api, People, { headers: this.headers })
    .pipe(map(data => data));
}

updatePeople(People) {
  // TODO: obtener token
  // TODO: not null
  const PeopleId = People.PeopleId;
  const token = this.authService.getToken();
  const url_api = `http://localhost:3000/api/Peoples/${PeopleId}/?access_token=${token}`;
  return this.http
    .put<PeopleInterface>(url_api, People, { headers: this.headers })
    .pipe(map(data => data));
}

deletePeople(id: string) {
  // TODO: obtener token
  // TODO: not null
  const token = this.authService.getToken();
  console.log(token);
  const url_api = `http://localhost:3000/api/Peoples/${id}/?access_token=${token}`;
  return this.http
    .delete<PeopleInterface>(url_api, { headers: this.headers })
    .pipe(map(data => data));
}
}
