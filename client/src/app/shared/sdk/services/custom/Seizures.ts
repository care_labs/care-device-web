/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';

// JDG: TTD: Need to build the below file automatically also; current one is in the
// wrong place, as it was hand edited. Should go in ../../models so it's in
// machine generated folder (SDK)
import { SeizuresInterface } from '../../../../models/Seizures-interface';

import { AuthService } from '../../../../services/auth.service';
/**
 * Api services for the `Seizures` model.

  JDG: Modified per the "Seizuress" sample; @Mean expert wasn't generating all HTTP verbs.
 */
@Injectable({
  providedIn: 'root'
})
export class SeizuresApiService {

  constructor( private http: HttpClient, private authService: AuthService) {
  }

  Seizures: Observable<any>;
  Seizuress: Observable<any>;
  
  public selectedSeizures: SeizuresInterface = {
    
    id: 0,
    date: '',
    frequency: 0,
    caffeine: 0,
    medicationsSkipped: 0,
    abnormalLowSleep: 0,
    accountId: 0,
    createdOn: new Date(0),
    tonicsClonics: 0,
    tonics: 0,
    partials: 0,
    absence: 0,
    others: 0,
    seizuresFiveMinute: 0,
    lastCloudsync: new Date(0),
    idCloud: 0,
      } ;

headers: HttpHeaders = new HttpHeaders({
  'Content-Type': 'application/json',
  Authorization: this.authService.getToken()
});

getAllSeizures () {
  const url_api = `http://localhost:3000/api/Seizures`;
  return this.http.get(url_api);
}
getSeizuresById (id: string) {
  const url_api = `http://localhost:3000/api/Seizuress/${id}`;
  return (this.Seizures = this.http.get(url_api));
}

saveSeizures(Seizures: SeizuresInterface) {
  // TODO: obtain token
  // TODO: not null
  const token = this.authService.getToken();
  const url_api = `http://localhost:3000/api/Seizuress?access_token=${token}`;
  return this.http
    .post<SeizuresInterface>(url_api, Seizures, { headers: this.headers })
    .pipe(map(data => data));
}

updateSeizures(Seizures) {
  // TODO: obtener token
  // TODO: not null
  const SeizuresId = Seizures.SeizuresId;
  const token = this.authService.getToken();
  const url_api = `http://localhost:3000/api/Seizuress/${SeizuresId}/?access_token=${token}`;
  return this.http
    .put<SeizuresInterface>(url_api, Seizures, { headers: this.headers })
    .pipe(map(data => data));
}

deleteSeizures(id: string) {
  // TODO: obtener token
  // TODO: not null
  const token = this.authService.getToken();
  console.log(token);
  const url_api = `http://localhost:3000/api/Seizuress/${id}/?access_token=${token}`;
  return this.http
    .delete<SeizuresInterface>(url_api, { headers: this.headers })
    .pipe(map(data => data));
}
}
