/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';

// JDG: TTD: Need to build the below file automatically also; current one is in the
// wrong place, as it was hand edited. Should go in ../../models so it's in
// machine generated folder (SDK)
import { DeviceInfoInterface } from '../../../../models/DeviceInfo-interface';

import { AuthService } from '../../../../services/auth.service';
/**
 * Api services for the `DeviceInfo` model.

  JDG: Modified per the "DeviceInfos" sample; @Mean expert wasn't generating all HTTP verbs.
 */
@Injectable({
  providedIn: 'root'
})
export class DeviceInfoApiService {

  constructor( private http: HttpClient, private authService: AuthService) {
  }

  DeviceInfo: Observable<any>;
  DeviceInfos: Observable<any>;
  
  public selectedDeviceInfo: DeviceInfoInterface = {
    
    id: '',
    name: '',
    hostname: '',
    cpuid: '',
    partno: '',
    manufactured: new Date(0),
    lot: '',
    udi: '',
    addr1: '',
    addr2: '',
    city: '',
    postalcode: '',
    country: '',
    timezone: '',
    alertingenabled: '',
    sensitivity: 0,
    reportTime: 0,
    rectimeMin: 0,
    reportAfter: 0,
    rectimePre: 0,
    debug: 0,
    localIpv4: 0,
    localIpv6: 0,
    lastUpdate: new Date(0),
    lastCloudsync: new Date(0),
    idCloud: 0,
      } ;

headers: HttpHeaders = new HttpHeaders({
  'Content-Type': 'application/json',
  Authorization: this.authService.getToken()
});

getAllDeviceInfo () {
  const url_api = `http://localhost:3000/api/DeviceInfos`;
  return this.http.get(url_api);
}
getDeviceInfoById (id: string) {
  const url_api = `http://localhost:3000/api/DeviceInfos/${id}`;
  return (this.DeviceInfo = this.http.get(url_api));
}

saveDeviceInfo(inDeviceInfo: DeviceInfoInterface) {
  // TODO: obtain token
  // TODO: not null
  const token = this.authService.getToken();
  console.log("DIAG: client\src\app\shared\sdk\services\custom\DeviceInfo.ts Writiting device info for save:", inDeviceInfo);
  const url_api = `http://localhost:3000/api/DeviceInfos?access_token=${token}`;
  return this.http
    .post<DeviceInfoInterface>(url_api, inDeviceInfo, { headers: this.headers })
    .pipe(map(data => data));
}

updateDeviceInfo(inDeviceInfo) {
  // TODO: obtener token
  // TODO: not null
  const DeviceInfoId = inDeviceInfo.DeviceInfoId;
  const token = this.authService.getToken();
  console.log("DIAG: Writiting device info for update:", inDeviceInfo, "in client\src\app\shared\sdk\services\custom\DeviceInfo.ts");
  const url_api = `http://localhost:3000/api/DeviceInfos/${DeviceInfoId}/?access_token=${token}`;
  return this.http
    .put<DeviceInfoInterface>(url_api, inDeviceInfo, { headers: this.headers })
    .pipe(map(data => data));
}

deleteDeviceInfo(id: string) {
  // TODO: obtener token
  // TODO: not null
  const token = this.authService.getToken();
  console.log(token);
  const url_api = `http://localhost:3000/api/DeviceInfos/${id}/?access_token=${token}`;
  return this.http
    .delete<DeviceInfoInterface>(url_api, { headers: this.headers })
    .pipe(map(data => data));
}
}
