/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';

// JDG: TTD: Need to build the below file automatically also; current one is in the
// wrong place, as it was hand edited. Should go in ../../models so it's in
// machine generated folder (SDK)
import { XethruSleepDetectionInterface } from '../../../../models/XethruSleepDetection-interface';

import { AuthService } from '../../../../services/auth.service';
/**
 * Api services for the `XethruSleepDetection` model.

  JDG: Modified per the "XethruSleepDetections" sample; @Mean expert wasn't generating all HTTP verbs.
 */
@Injectable({
  providedIn: 'root'
})
export class XethruSleepDetectionApiService {

  constructor( private http: HttpClient, private authService: AuthService) {
  }

  XethruSleepDetection: Observable<any>;
  XethruSleepDetections: Observable<any>;
  
  public selectedXethruSleepDetection: XethruSleepDetectionInterface = {
    
    id: 0,
    userId: 0,
    timeStamp: new Date(0),
    breathingState: 0,
    state1Time: 0,
    breathingRate: 0,
    hasArmMoved: 0,
    movementTime: 0,
    movementRepeating: 0,
    lastCloudsync: new Date(0),
    idCloud: 0,
      } ;

headers: HttpHeaders = new HttpHeaders({
  'Content-Type': 'application/json',
  Authorization: this.authService.getToken()
});

getAllXethruSleepDetection () {
  const url_api = `http://localhost:3000/api/XethruSleepDetection`;
  return this.http.get(url_api);
}
getXethruSleepDetectionById (id: string) {
  const url_api = `http://localhost:3000/api/XethruSleepDetections/${id}`;
  return (this.XethruSleepDetection = this.http.get(url_api));
}

saveXethruSleepDetection(XethruSleepDetection: XethruSleepDetectionInterface) {
  // TODO: obtain token
  // TODO: not null
  const token = this.authService.getToken();
  const url_api = `http://localhost:3000/api/XethruSleepDetections?access_token=${token}`;
  return this.http
    .post<XethruSleepDetectionInterface>(url_api, XethruSleepDetection, { headers: this.headers })
    .pipe(map(data => data));
}

updateXethruSleepDetection(XethruSleepDetection) {
  // TODO: obtener token
  // TODO: not null
  const XethruSleepDetectionId = XethruSleepDetection.XethruSleepDetectionId;
  const token = this.authService.getToken();
  const url_api = `http://localhost:3000/api/XethruSleepDetections/${XethruSleepDetectionId}/?access_token=${token}`;
  return this.http
    .put<XethruSleepDetectionInterface>(url_api, XethruSleepDetection, { headers: this.headers })
    .pipe(map(data => data));
}

deleteXethruSleepDetection(id: string) {
  // TODO: obtener token
  // TODO: not null
  const token = this.authService.getToken();
  console.log(token);
  const url_api = `http://localhost:3000/api/XethruSleepDetections/${id}/?access_token=${token}`;
  return this.http
    .delete<XethruSleepDetectionInterface>(url_api, { headers: this.headers })
    .pipe(map(data => data));
}
}
