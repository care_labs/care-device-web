/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';

// JDG: TTD: Need to build the below file automatically also; current one is in the
// wrong place, as it was hand edited. Should go in ../../models so it's in
// machine generated folder (SDK)
import { CamDetectionInterface } from '../../../../models/CamDetection-interface';

import { AuthService } from '../../../../services/auth.service';
/**
 * Api services for the `CamDetection` model.

  JDG: Modified per the "CamDetections" sample; @Mean expert wasn't generating all HTTP verbs.
 */
@Injectable({
  providedIn: 'root'
})
export class CamDetectionApiService {

  constructor( private http: HttpClient, private authService: AuthService) {
  }

  CamDetection: Observable<any>;
  CamDetections: Observable<any>;
  
  public selectedCamDetection: CamDetectionInterface = {
    
    id: 0,
    userId: 0,
    timeStamp: new Date(0),
    recDuration: new Date(0),
    recFilename: '',
    lastCloudsync: new Date(0),
    idCloud: 0,
      } ;

headers: HttpHeaders = new HttpHeaders({
  'Content-Type': 'application/json',
  Authorization: this.authService.getToken()
});

getAllCamDetection () {
  const url_api = `http://localhost:3000/api/CamDetection`;
  return this.http.get(url_api);
}
getCamDetectionById (id: string) {
  const url_api = `http://localhost:3000/api/CamDetections/${id}`;
  return (this.CamDetection = this.http.get(url_api));
}

saveCamDetection(CamDetection: CamDetectionInterface) {
  // TODO: obtain token
  // TODO: not null
  const token = this.authService.getToken();
  const url_api = `http://localhost:3000/api/CamDetections?access_token=${token}`;
  return this.http
    .post<CamDetectionInterface>(url_api, CamDetection, { headers: this.headers })
    .pipe(map(data => data));
}

updateCamDetection(CamDetection) {
  // TODO: obtener token
  // TODO: not null
  const CamDetectionId = CamDetection.CamDetectionId;
  const token = this.authService.getToken();
  const url_api = `http://localhost:3000/api/CamDetections/${CamDetectionId}/?access_token=${token}`;
  return this.http
    .put<CamDetectionInterface>(url_api, CamDetection, { headers: this.headers })
    .pipe(map(data => data));
}

deleteCamDetection(id: string) {
  // TODO: obtener token
  // TODO: not null
  const token = this.authService.getToken();
  console.log(token);
  const url_api = `http://localhost:3000/api/CamDetections/${id}/?access_token=${token}`;
  return this.http
    .delete<CamDetectionInterface>(url_api, { headers: this.headers })
    .pipe(map(data => data));
}
}
