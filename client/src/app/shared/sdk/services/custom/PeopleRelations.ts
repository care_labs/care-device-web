/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';

// JDG: TTD: Need to build the below file automatically also; current one is in the
// wrong place, as it was hand edited. Should go in ../../models so it's in
// machine generated folder (SDK)
import { PeopleRelationsInterface } from '../../../../models/PeopleRelations-interface';

import { AuthService } from '../../../../services/auth.service';
/**
 * Api services for the `PeopleRelations` model.

  JDG: Modified per the "PeopleRelationss" sample; @Mean expert wasn't generating all HTTP verbs.
 */
@Injectable({
  providedIn: 'root'
})
export class PeopleRelationsApiService {

  constructor( private http: HttpClient, private authService: AuthService) {
  }

  PeopleRelations: Observable<any>;
  PeopleRelationss: Observable<any>;
  
  public selectedPeopleRelations: PeopleRelationsInterface = {
    
    patientId: 0,
    relationId: 0,
    role: '',
    authorize: '',
    lastmodified: new Date(0),
    lastCloudsync: new Date(0),
    idCloud: 0,
    id: 0,
      } ;

headers: HttpHeaders = new HttpHeaders({
  'Content-Type': 'application/json',
  Authorization: this.authService.getToken()
});

getAllPeopleRelations () {
  const url_api = `http://localhost:3000/api/PeopleRelations`;
  return this.http.get(url_api);
}
getPeopleRelationsById (id: string) {
  const url_api = `http://localhost:3000/api/PeopleRelationss/${id}`;
  return (this.PeopleRelations = this.http.get(url_api));
}

savePeopleRelations(PeopleRelations: PeopleRelationsInterface) {
  // TODO: obtain token
  // TODO: not null
  const token = this.authService.getToken();
  const url_api = `http://localhost:3000/api/PeopleRelationss?access_token=${token}`;
  return this.http
    .post<PeopleRelationsInterface>(url_api, PeopleRelations, { headers: this.headers })
    .pipe(map(data => data));
}

updatePeopleRelations(PeopleRelations) {
  // TODO: obtener token
  // TODO: not null
  const PeopleRelationsId = PeopleRelations.PeopleRelationsId;
  const token = this.authService.getToken();
  const url_api = `http://localhost:3000/api/PeopleRelationss/${PeopleRelationsId}/?access_token=${token}`;
  return this.http
    .put<PeopleRelationsInterface>(url_api, PeopleRelations, { headers: this.headers })
    .pipe(map(data => data));
}

deletePeopleRelations(id: string) {
  // TODO: obtener token
  // TODO: not null
  const token = this.authService.getToken();
  console.log(token);
  const url_api = `http://localhost:3000/api/PeopleRelationss/${id}/?access_token=${token}`;
  return this.http
    .delete<PeopleRelationsInterface>(url_api, { headers: this.headers })
    .pipe(map(data => data));
}
}
