export interface DeviceWifiInterface {
  devId?: number;
  id?: number;
  passphrase?: string;
  ssid?: string;
  type?: string;
  dhcp?: number;
  proxy?: number;
  staticip?: number;
  proxyPort?: number;
  proxyHost?: string;
  lastcloudsync?: Date;
  idCloud?: number;
}
