export interface DeviceInfoInterface {
  id?: string;
  name?: string;
  hostname?: string;
  cpuid?: string;
  partno?: string;
  manufactured?: Date;
  lot?: string;
  udi?: string;
  addr1?: string;
  addr2?: string;
  city?: string;
  postalcode?: string;
  country?: string;
  timezone?: string;
  alertingenabled?: string;
  sensitivity?: number;
  reportTime?: number;
  rectimeMin?: number;
  reportAfter?: number;
  rectimePre?: number;
  debug: number;
  localIpv4?: number;
  localIpv6?: number;
  lastUpdate?: Date;
  lastCloudsync?: Date;
  idCloud?: number;
}
