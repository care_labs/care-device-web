export interface XethruSleepDetectionInterface {
  id?: number;
  userId?: number;
  timeStamp?: Date;
  breathingState?: number;
  state1Time?: number;
  breathingRate?: number;
  hasArmMoved?: number;
  movementTime?: number;
  movementRepeating?: number;
  lastCloudsync?: Date;
  idCloud?: number;
}

