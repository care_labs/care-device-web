export interface XethruDetectionInterface {
  id?: number;
  userId?: number;
  timeStamp?: Date;
  movementRepeating?: number;
  movementTime?: number;
  hasLegMoved?: number;
  lastCloudsync?: Date;
  idCloud?: number;
}
