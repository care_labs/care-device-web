export interface IncidentsInterface {
  id?: number;
  date?: Date;
  frequency?: number;
  caffeine?: number;
  medicationsSkipped?: number;
  abnormalLowSleep?: number;
  accountId?: number;
  createdOn?: Date;
  tonicsClonics?: number;
  tonics?: number;
  partials?: number;
  absence?: number;
  others?: number;
  seizuresFiveMinute?: number;
  notes?: string;
  duration?: string;
  accuracy?: number;
  averageHr?: number;
  averageRespiration?: number;
  gazeCaptured?: number;
  gazeAverage?: number;
  limbMotion?: number;
  lastCloudsync?: Date;
  idCloud?: number;
}
