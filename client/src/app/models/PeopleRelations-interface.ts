export interface PeopleRelationsInterface {
  patientId?: number;
  relationId?: number;
  role?: string;
  authorize?: string;
  lastmodified?: Date;
  lastCloudsync?: Date;
  idCloud?: number;
  id?: number;
}

