export interface CamDetectionInterface {
  id?: number;
  userId?: number;
  timeStamp?: Date;
  recDuration?: Date;
  recFilename?: string;
  lastCloudsync?: Date;
  idCloud?: number;
}
