export interface SeizuresInterface {
  id?: number;
  date?: string;
  frequency?: number;
  caffeine?: number;
  medicationsSkipped?: number;
  abnormalLowSleep?: number;
  accountId?: number;
  createdOn?: Date;
  tonicsClonics?: number;
  tonics?: number;
  partials?: number;
  absence?: number;
  others?: number;
  seizuresFiveMinute?: number;
  lastCloudsync?: Date;
  idCloud?: number;
}
