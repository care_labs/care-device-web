export interface PeopleInterface {
  accountId?: number;
  email?: string;
  firstName?: string;
  lastName?: string;
  password?: string;
  dob?: string;
  clinicalTrials?: number;
  productsServices?: number;
  invocationFirst?: number;
  timezone?: string;
  alertsOn?: number;
  smartphone?: string;
  voiceonlyphone?: string;
  lastEdit?: Date;
  lastCloudsync?: Date;
  idCloud?: number;
}
