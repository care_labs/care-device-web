export interface O2vibeConnectionInterface {
  id?: number;
  userId?: number;
  timeStamp?: Date;
  heartRate?: number;
  spo2Rate?: number;
  lastCloudsync?: Date;
  idCloud?: number;
}
