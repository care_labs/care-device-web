export interface MobileConnectionInterface {
  id?: number;
  userId?: number;
  timeStamp?: Date;
  heartRate?: number;
  accelerationx?: number;
  accelerationy?: number;
  accelerationz?: number;
  seizureDetected?: number;
  lastCloudsync?: Date;
  idCloud?: number;
}

