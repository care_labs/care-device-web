import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes} from '@angular/router';

import { IncidentsComponent } from './components/incidents/incidents.component';

//various sensor input pages; these build Incidents
import { CamerainfoComponent } from './components/camerainfo/camerainfo.component';
import { RadarinfoComponent } from './components/radarinfo/radarinfo.component';
import { WearableinfoComponent } from './components/wearableinfo/wearableinfo.component';

import { DevicesetupComponent } from './components/devicesetup/devicesetup.component';
import { NetsetupComponent } from './components/netsetup/netsetup.component';
import { PersonsetupComponent } from './components/personsetup/personsetup.component';
import { PeoplelistComponent } from './components/peoplelist/peoplelist.component';
import { PatientsetupComponent } from './components/patientsetup/patientsetup.component';
import { SupportComponent } from './components/support/support.component';
import { AboutComponent } from './components/about/about.component';
import { HomeComponent } from './components/home/home.component';
import { NotFoundComponent } from './components/not-found/not-found.component';

const appRoutes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'incidents', component: IncidentsComponent },
  { path: 'radarinfo', component: RadarinfoComponent },
  { path: 'camerainfo', component: CamerainfoComponent },
  { path: 'wearableinfo', component: WearableinfoComponent },
  { path: 'devicesetup', component: DevicesetupComponent },
  { path: 'netsetup', component: NetsetupComponent },
  { path: 'peoplelist', component: PeoplelistComponent},
  { path: 'patientsetup', component: PatientsetupComponent},
  { path: 'personsetup/:id', component: PersonsetupComponent },
  { path: 'support', component: SupportComponent },
  { path: 'about', component: AboutComponent },
  { path: '**', component: NotFoundComponent}
  ]

@NgModule({
  declarations: [],
   imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
