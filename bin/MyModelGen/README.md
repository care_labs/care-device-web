LoopBack SDK Builder 
==================
> Disclaimer: This project is not longer directly extended or improved by the author Jonathan Casarrubias, though any PRs improving or extending the SDK Builder are continuously being accepted and integrated in a weekly basis. Therefore, this module keeps growing as long as the community keeps sending patches.

The [@mean-expert/loopback-sdk-builder](https://www.npmjs.com/package/@mean-expert/loopback-sdk-builder) is a community driven module forked from the official `loopback-sdk-angular` and refactored to support [Angular 2+](http://angular.io).

The [LoopBack SDK Builder](https://www.npmjs.com/package/@mean-expert/loopback-sdk-builder) will explore your [LoopBack Application](http://loopback.io) and will automatically build everything you need to start writing your [Angular 2 Applications](http://angular.io) right away. From Interfaces and Models to API Services and Real-time communications.

`NOTE: This sdk builder is not compatible with LoopBack 4.`

# Installation

````sh
$ cd to/loopback/project
$ npm install --save-dev @mean-expert/loopback-sdk-builder
````

# Documentation

[LINK TO WIKI DOCUMENTATION](https://github.com/mean-expert-official/loopback-sdk-builder/wiki)

# Contribute
Most of the PRs fixing any reported issue or adding a new functionality are being accepted.

Use the development branch to create a new branch from. If adding new features a new unit test will be expected, though most of the patches nowadays are small fixes or tweaks that usually won't require a new test.

# OIDC-SSO Service
A new campaing to call developers to register as beta testers for the [OnixJS](https://onixjs.io) Single Sign-On Service is active now. This campaing will be running during the month of June 2018, allowing all of those registered users to have early access during closed beta.

- _Closed beta access will be active starting from July 2018._

Register now and get the chance to have an unlimited annual enterprise membership for free.

[[REQUEST EARLY ACCESS HERE](https://onixjs.io)]

**Technology References:**

- **OnixJS**: Enterprise Grade NodeJS Platform implementing Industry Standards & Patterns in order to provide the best Connectivity, Stability, High-Availability and High-Performance.
- **Single Sign-On (SSO)**: Is a property of access control of multiple related, yet independent, software systems. With this property, a user logs in with a single ID and password to gain access to a connected system or systems without using different usernames or passwords, or in some configurations seamlessly sign on at each system.
- **OpenID Connect (OIDC)**: OpenID Connect 1.0 is a simple identity layer on top of the OAuth 2.0 protocol. It allows Clients to verify the identity of the End-User based on the authentication performed by an Authorization Server, as well as to obtain basic profile information about the End-User in an interoperable and REST-like manner.

# Contact
Discuss features and ask questions on [@johncasarrubias at Twitter](https://twitter.com/johncasarrubias).

### Modifications for MALoNE CRUD.

I have made a form of the above project, to go with what I call "Malone" or Mysql Angular LOopback Node Express.

Whew.

While investigating these different frameworks, they all seem like they make simple operations like CRUD well, simple.

They are not. There aren't a lot of good samples out there, and the ones that are, are quite often AngularJS, or Loopback 2, etc.

You get the drift. With the Internet, cruft builds up, and great content like "405 The Movie" and "Terribly Terry Tate" are hard to find.

Let's move on! 

## Design goals

I want to make it as easy as possible to take a database, and build a front end application to edit that database.

I think the reason these projects tend to be hard to find is that just editing a database typically isn't the real business logic (unless it's just basic data collection). This doesn't mean that 80% of your code *isn't* for that, however. So let's make those parts easy.

## How we do that

We will build simple grid (TODO: Parent/Child, one to many, etc) HTML that you can paste into your Angular front end files.

We will also do auto-generate of the database files to use in your LoopBack/Express project.

## Recommended Usage

The first time you run the tool, you should make a copy of all of the HTML files. THEN make a copy to edit.
