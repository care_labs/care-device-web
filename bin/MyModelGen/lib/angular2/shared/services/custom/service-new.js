/* tslint:disable */
<%- buildServiceImports(model, loadAccessToken, isIo) %>
/**
 * Api services for the `<%-: modelName %>` model.
<% if ( model.description ){ -%>
 *
 * **Details**
 *
 * <%-: model.description | replace:/\n/gi, '\n * ' %>
<% } -%>

  JDG: Modified per the "<%-: modelName %>s" sample; @Mean expert wasn't generating all HTTP verbs.
 */
@Injectable()
export class <%-: modelName %> Api extends BaseLoopBackApi {

  constructor( private http: HttpClient, private authService:AuthService) {
  }

  <%-: modelName %>: Observable<any>;
  <%-: modelName %>s: Observable<any>;

  public selected <%-: modelName %>: <%-: modelName %>Interface = {

    <% for (var prop in model.properties) { %>
    <%= prop %>: <% - buildPropertyDefaultValue(model.properties[prop]) %><% } %>,
      <% } %>
      };

headers: HttpHeaders = new HttpHeaders({
  'Content-Type': 'application/json',
  Authorization: this.authService.getToken()
});

getAll<%-: modelName %> () {
  const url_api = `http://localhost:3000/api/<%-: modelName %>`;
  return this.http.get(url_api);
}
get<%-: modelName %>ById<%-: modelName %> (id: string) {
  const url_api = `http://localhost:3000/api/<%-: modelName %>s/${id}`;
  return (this.<%-: modelName %> = this.http.get(url_api));
}

save<%-: modelName %>(<%-: modelName %>: <%-: modelName %>Interface) {
  // TODO: obtener token
  // TODO: not null
  const token = this.authService.getToken();
  const url_api = `http://localhost:3000/api/<%-: modelName %>s?access_token=${token}`;
  return this.http
    .post<<%-: modelName %>Interface>(url_api, <%-: modelName %>, { headers: this.headers })
    .pipe(map(data => data));
}

update<%-: modelName %>(<%-: modelName %>) {
  // TODO: obtener token
  // TODO: not null
  const <%-: modelName %>Id = <%-: modelName %>.<%-: modelName %>Id;
  const token = this.authService.getToken();
  const url_api = `http://localhost:3000/api/<%-: modelName %>s/${<%-: modelName %>Id}/?access_token=${token}`;
  return this.http
    .put<<%-: modelName %>Interface>(url_api, <%-: modelName %>, { headers: this.headers })
    .pipe(map(data => data));
}

delete<%-: modelName %>(id: string) {
  // TODO: obtener token
  // TODO: not null
  const token = this.authService.getToken();
  console.log(token);
  const url_api = `http://localhost:3000/api/<%-: modelName %>s/${id}/?access_token=${token}`;
  return this.http
    .delete<<%-: modelName %>Interface>(url_api, { headers: this.headers })
    .pipe(map(data => data));
}
}
