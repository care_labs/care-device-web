var path = require('path');

var app = require(path.resolve(__dirname, '../server'));
var ds = app.datasources.accountDS;

// try
// {
  ds.discoverAndBuildModels('Account', { schema: 'loopback-example-mysql', relations: true },
    function (err, models)
    {
      if (err) throw err;

      models.Account.find(function(err, accounts)
      {
        if (err) throw err;

        console.log('Found:', accounts);

        ds.disconnect();
      });
    });
// }
// catch (error)
// {
//   console.error(error);
// }