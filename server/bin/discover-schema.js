'use strict';

//  JDG: A stand-alone node.js file to build our
//  metacode to access the database. This builds the classes
//  dynamically; this way, we can change the database and 
//  our code will reflect the correct / updated datebase.

//  import the API's we need

const loopback = require('loopback');
const promisify = require('util').promisify;
const fs = require('fs');
const writeFile = promisify(fs.writeFile);
const readFile = promisify(fs.readFile);
const mkdirp = promisify(require('mkdirp'));

//  Initialize / create our datasource. We're doing this explicitly here,
//  as we won't have access to the Loopback ecosystem.
//  however, for robustness, we will use the datasources.json file.
//  this way, the generation code, as well as the actual API to access
//  the database, will be consistent.

const DATASOURCE_DEVICE = 'healthcare_db';
const DATASOURCE_USERS = 'users_db';
//relative to the location of this source file (apparently)
console.log("Loading Loopback");
const dataSourceConfig = require('../../server/datasources.json');
const healthcare_db = new loopback.DataSource(dataSourceConfig[DATASOURCE_DEVICE]);
console.log("Loaded DeviceDB");
const users_db = new loopback.DataSource(dataSourceConfig[DATASOURCE_USERS]);
console.log("Loaded UsersDB");

// Node.js does not allow the main module function to be async;
// so, this top-level code cannot use await keyword for flow control. 
// So we need to create the discovery code in a helper function 
// that’s executed via the Promise API.

discover().then(
  success => process.exit(),
  error => { console.error('UNHANDLED ERROR:\n', error); process.exit(1); },
);

async function discover() {
  // It's important to pass the same "options" object to all calls
  // of dataSource.discoverSchemas(), it allows the method to cache
  // discovered related models
  const options = { relations: true };

  // Discover models and relations
  // DiscoverSchemas creates a model with this specific name.
  const camDetection = await healthcare_db.discoverSchemas('cam_detection', options);
  const deviceInfo = await healthcare_db.discoverSchemas('device_info', options);
  const deviceWifi = await healthcare_db.discoverSchemas('device_wifi', options);
  const incidents = await healthcare_db.discoverSchemas('incidents', options);
  const mobileConnection = await healthcare_db.discoverSchemas('mobile_connection', options);
  const o2vibeConnection = await healthcare_db.discoverSchemas('o2vibe_connection', options);
  const seizures = await healthcare_db.discoverSchemas('seizures', options);
  const xethruDetection = await healthcare_db.discoverSchemas('xethru_detection', options);
  const xethruSleepDetection = await healthcare_db.discoverSchemas('xethru_sleep_detection', options);

  //  now for the users database. Note we won't segregate teh schemas once loaded;
  //  for HIPAA that's only for persistant data. 
  //  jdg: 'People' was created but wasn't in the design spec that was routed.
  //  so, I'm commenting it out; it's basically deprecated.
  //    const peopleSchema = await db.discoverSchemas('people', options);
  const people = await users_db.discoverSchemas('people', options);
  const peopleRelations = await users_db.discoverSchemas('people_relations', options);

  //   Create model definition files
  //  jdg: todo: is there a way to iterate through all of these without having
  //  to code this all out?
  //  SQL is similar to:
  //  'SELECT table_name AS "tableName", <etc> FROM information_schema.columns WHERE ...

  await mkdirp('common/models');

  await writeFile(
    'common/models/camDetection.json',
    JSON.stringify(camDetection['healthcare_db.cam_detection'], null, 2)
  );
  await writeFile(
    'common/models/deviceInfo.json',
    JSON.stringify(deviceInfo['healthcare_db.device_info'], null, 2)
  );
  await writeFile(
    'common/models/deviceWifi.json',
    JSON.stringify(deviceWifi['healthcare_db.device_wifi'], null, 2)
  );
  await writeFile(
    'common/models/incidents.json',
    JSON.stringify(incidents['healthcare_db.incidents'], null, 2)
  );
  await writeFile(
    'common/models/mobileConnection.json',
    JSON.stringify(mobileConnection['healthcare_db.mobile_connection'], null, 2)
  );
  await writeFile(
    'common/models/o2vibeConnection.json',
    JSON.stringify(o2vibeConnection['healthcare_db.o2vibe_connection'], null, 2)
  );
  await writeFile(
    'common/models/seizures.json',
    JSON.stringify(seizures['healthcare_db.seizures'], null, 2)
  );
  await writeFile(
    'common/models/xethruDetection.json',
    JSON.stringify(xethruDetection['healthcare_db.xethru_detection'], null, 2)
  );
  await writeFile(
    'common/models/xethruSleepDetection.json',
    JSON.stringify(xethruSleepDetection['healthcare_db.xethru_sleep_detection'], null, 2)
  );

  await writeFile(
    'common/models/people.json',
    JSON.stringify(people['users_db.people'], null, 2)
  );
  // await writeFile(
  //   'common/models/users_dbFullDump.txt',
  //   JSON.stringify(usersSchema)
  // );
  await writeFile(
    'common/models/peopleRelations.json',
    JSON.stringify(peopleRelations['users_db.people_relations'], null, 2)
  );

  // Expose models via REST API
  const configJson = await readFile('server/model-config.json', 'utf-8');
  console.log('MODEL CONFIG', configJson);
  const config = JSON.parse(configJson);

  config.people = { dataSource: DATASOURCE_USERS, public: true };
  config.peopleRelations = { dataSource: DATASOURCE_USERS, public: true };

  config.camDetection = { dataSource: DATASOURCE_DEVICE, public: true };
  config.deviceInfo = { dataSource: DATASOURCE_DEVICE, public: true };
  config.deviceWifi = { dataSource: DATASOURCE_DEVICE, public: true };
  config.mobileConnection = { dataSource: DATASOURCE_DEVICE, public: true };
  config.o2vibeConnection = { dataSource: DATASOURCE_DEVICE, public: true };
  config.seizures = { dataSource: DATASOURCE_DEVICE, public: true };
  config.incidents = { dataSource: DATASOURCE_DEVICE, public: true };
  config.xethruDetection = { dataSource: DATASOURCE_DEVICE, public: true };
  config.xethruSleepDetection = { dataSource: DATASOURCE_DEVICE, public: true };
  await writeFile(
    'server/model-config.json',
    JSON.stringify(config, null, 2)
  );
}
