'use strict';
module.exports = function(deviceInfo) {

  // Model.remoteMethod(requestHandlerFunctionName, [options])
  // afterInitialize is a model hook which is still used in loopback
  // all other model hooks are depricated, use the .observer (string) BS
  // See:https://loopback.io/doc/en/lb3/Operation-hooks.html

  deviceInfo.afterInitialize = function () {
    // http://docs.strongloop.com/display/public/LB/Model+hooks#Modelhooks-afterInitialize
    console.log('DIAG: DeviceInfo: afterInitialize triggered', this);
 
  };

  // the rest are all operation hooks
  // - http://docs.strongloop.com/display/public/LB/Operation+hooks
  // model operation hook

  deviceInfo.observe('before save', function (ctx, next) {
    if (ctx.instance) {
      console.log('DIAG: DeviceInfo:About to save a deviceInfo instance:', ctx.instance, next);
    } else {
      console.log('DIAG: DeviceInfo:About to update deviceInfos that match the query %j:', ctx.where);
    }
    next();
  });

  deviceInfo.observe('before save', function (ctx, next) {
    if (ctx.instance) {
      console.log('DIAG: DeviceInfo:About to save a deviceInfo instance:', ctx.instance, next);
    } else {
      console.log('DIAG: DeviceInfo:About to update deviceInfos that match the query %j:', ctx.where);
    }
    next();
  });

  deviceInfo.observe('after save', function (ctx, next) {
    console.log('DIAG: DeviceInfo: after save triggered:', ctx.Model.modelName, ctx.instance);
    next();
  });
  deviceInfo.observe('before delete', function (ctx, next) {
    console.log('DIAG: DeviceInfo: before delete triggered:',
      ctx.Model.modelName, ctx.instance);
    next();
  });
  deviceInfo.observe('after delete', function (ctx, next) {
    console.log('DIAG: DeviceInfo: after delete triggered:',
      ctx.Model.modelName, (ctx.instance || ctx.where));
    next();
  });


}
